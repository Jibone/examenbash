#!/bin/bash
#descargamos el zip, creamos su proximo destino, lo descomprimimos dentro, y luego nos movemos hacia ese mismo directorio.
wget https://icb.utalca.cl/~fduran/thrombin-ligand.zip
mkdir $HOME/examenBASH
unzip thrombin-ligand.zip -d $HOME/examenBASH
clear
cd $HOME/examenBASH

#utilizamos un ls para listar los archivos .mol, aparte de práctico encuentro que queda bonito.
echo "----------------Bienvenido al comparador de moleculas----------------"
echo "Actualmente se encuentran los siguientes archivos .mol para comparar:"
ls

#se le pide al usuario que ingrese el nombre.mol (separados por un espacio) de los archivos a revisar.
echo "Ingrese el nombre de los archivos a comparar (ejemplo: archivo_1.mol archivo_2.mol)"
read archivo1 archivo2

#conseguimos la cantidad de atomos de cada .mol de acuerdo a su counts line, lo mismo con los bonds de ambos archivos. (utilizamos awk para escoger la columna deseada)
atoms1=`cat $archivo1 |grep 'V2000$'|awk '{print $1}'`
atoms2=`cat $archivo2 |grep 'V2000$'|awk '{print $1}'`
bonds1=`cat $archivo1 |grep 'V2000$'|awk '{print $2}'`
bonds2=`cat $archivo2 |grep 'V2000$'|awk '{print $2}'`

#aquí se compara la cantidad de atomos y bonds entre ambos archivos, utilizando if y elif
if [[ $atoms1 -eq $atoms2 && $bonds1 -eq $bonds2 ]]; then
	echo "Ambos ficheros presentan la misma cantidad de atoms y bonds"
elif [ $atoms1 -gt $atoms2 ]; then
	if [ $bonds1 -gt $bonds2 ]; then
		echo "El fichero $archivo1 presenta mas atoms y mas bonds que $archivo2"
	elif [ $bonds2 -gt $bonds1 ]; then
		echo "El fichero $archivo1 presenta mas atoms, pero menos bonds que $archivo2"
	fi
elif [ $atoms2 -gt $atoms1 ]; then
	if [ $bonds2 -gt $bonds1 ]; then
		echo "El fichero $archivo2 presenta mas atoms y mas bonds que $archivo1"
	elif [ $bonds1 -gt $bonds2 ]; then
		echo "El fichero $archivo2 presenta mas atoms, pero menos bonds $archivo1"
	fi
fi
#aqui se realiza una cuenta de los atomos y bonds de ambos archivos en funcion de la cantidad de lineas que presentan describiendo cada uno de estos mencionados. Con los atomos usamos $10 para la columna, ya que son los unicos que presentan una columna N°10, y con los bonds comparo con que su columna 4 y 5 sean iguales a 0, ya que solo los bonds tienen esa característica en los archivos. para ambos casos se cuentan las lineas con un awk, aumentando un contador por cada linea, siendo printeado este contador al terminar.
cant_atoms1=`cat $archivo1 | awk '$10 == 0' | awk '{nlines = nlines+1} END {print nlines}'`
cant_atoms2=`cat $archivo2 | awk '$10 == 0' | awk '{nlines = nlines+1} END {print nlines}'`
cant_bonds1=`cat $archivo1 | awk '$4 == 0' |awk '$5 == 0' | awk '{nlines = nlines+1} END {print nlines}'`
cant_bonds2=`cat $archivo2 | awk '$4 == 0' |awk '$5 == 0' | awk '{nlines = nlines+1} END {print nlines}'`

#aqui se compara la cantidad de atomos y bonds que dice la counts line con la cantidad de lineas en las que se referencia a cada uno, para ver si se presenta alguna "anomalía" en la molecula.
if [[ atoms1 -eq cant_atoms1 && bonds1 -eq cant_bonds1 ]]; then
	echo "La molecula $archivo1 NO presenta una anomalía (atoms y bonds coinciden con los indicados en counts line)"
else
	echo "La molecula $archivo1 SI presenta una anomalía (atoms y bonds no coinciden con los indicados en counts line)"
fi

if [[ atoms2 -eq cant_atoms2 && bonds2 -eq cant_bonds2 ]]; then
	echo "La molecula $archivo2 NO presenta una anomalía (atoms y bonds coinciden con los indicados en counts line)"
else
	echo "La molecula $archivo2 SI presenta una anomalía (atoms y bonds no coinciden con los indicados en counts line)"
fi

#esto lo usé para comparar el Counts Line con los atomos/bonds contados 1 por 1, pero lo dejaré comentado para que no estorbe con el programa.
#echo AtomosCL= $atoms1 AtomosBrutos= $cant_atoms1
#echo BondsCL= $bonds1 BondsBrutos= $cant_bonds1

#aquí conseguimos los atomos que hay, y cuantos de cada uno, para esto se usa primero un awk para separar el bloque de los atomos del resto, con el segundo se obtiene la columna 4 (esta contiene los atomos), el tercer comando ordena alfabeticamente esto, para poder aplicarle despues el uniq y pueda eliminar los repetidos correctamente, con el -c de uniq conseguimos la cuenta de cada uno de los atomos. y finalmente un awk para printear esto ordenado.
echo "Los atomos y la cantidad de estos que se pueden encontrar en $archivo1 son:"
cat $archivo1 | awk '$10 == 0' | awk '{print $4}' | sort | uniq -c | awk '{print $2 ": " $1}'

echo "Los atomos y la cantidad de estos que se pueden encontrar en $archivo2 son:"
cat $archivo2 | awk '$10 == 0' | awk '{print $4}' | sort | uniq -c | awk '{print $2 ": " $1}'

#aquí se le pregunta al usuario si desea visualizar la molecula, u obtener una imagen de esta. Se utiliza pymol.
echo "¿Desea visualizar la molecula de $archivo1? [s/n]"
read vis1
if [ $vis1 == "s" ]; then
	pymol $archivo1
fi
echo "¿Desea generar una imagen de la molecula de $archivo1? [s/n]"
read img1
if [ $img1 == "s" ]; then
	mkdir $HOME/examenBASH/img
	pymol -x -g $archivo1 #$HOME/examenBASH/img
	echo "Se ha guardado la imagen en $HOME/examenBASH/img"
	eog $HOME/examenBASH/img/$archivo1.png
fi

echo "¿Desea visualizar la molecula de $archivo2? [s/n]"
read vis2
if [ $vis2 == "s" ]; then
	pymol $archivo2
fi
echo "¿Desea generar una imagen de la molecula de $archivo2? [s/n]"
read img2
if [ $img2 == "s" ]; then
	mkdir $HOME/examenBASH/img
	pymol -x -g $archivo2 #$HOME/examenBASH/img
	echo "Se ha guardado la imagen en $HOME/examenBASH/img"
	eog $HOME/examenBASH/img/$archivo2.png
fi

echo "Creditos: José Bernal."




